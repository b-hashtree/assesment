import time
import sys
import random

class PythonA:

    def evaluateInt(map, logSize, shift):
        ellapsed=[]
        suma=0
        ellapsed.append(time.time_ns()/1e6)

        size = 1 << logSize
        numbers=[]
        for i in range(0,size):
            numbers.append(i*shift)
    
        random.shuffle(numbers)

        ellapsed.append(time.time_ns()/1e6)

        for i in range(0,size):
            key = numbers[i]
            map[key]=1/random.randint(1,9)
    
        ellapsed.append(time.time_ns()/1e6)

        for i in range(0,size):
            key = numbers[i]
            suma += map[key]
    
        ellapsed.append(time.time_ns()/1e6)
           
        
        for i in range(0,size):
            key = numbers[i]
            map.pop(key)          
        

        ellapsed.append(time.time_ns()/1e6)
        
        ellapsed.append(suma)

        return ellapsed
        

    
    


    map = {}
    logSize = int(sys.argv[1])
    shift = int(sys.argv[2])


    ellapsed = evaluateInt(map,logSize,shift)

    string = str(logSize) + "\t" + str(shift) + "\t"
    for x in range(1,len(ellapsed)):
            a = ellapsed[x]
            b = ellapsed[x-1]
            string+= str(a-b) + "\t"
    print(string)

