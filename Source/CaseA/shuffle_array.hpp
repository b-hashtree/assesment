
#include <cstdlib>
#include <algorithm>

#define RAND_SEED 33

template <typename Vector>
void shuffle_array(Vector& array)
{
    srand(RAND_SEED);
    for (int i = array.size() - 1; i > 0; i--)
    {
        int index = rand() % i;
        std::swap(array[index], array[i]);
    }
}
