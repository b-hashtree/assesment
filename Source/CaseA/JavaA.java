
import java.util.*;

public class JavaA {
	
	
	public static ArrayList<Double> evaluateInt(Map<Integer,Double> map, int logSize, int shift) {
        ArrayList<Double> ellapsed = new ArrayList<>();
        double suma=0;
        
        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Build Array
        int size = 1 << logSize; // 2 ** logSize
        int[] numbers = new int[size];
        for (int i = 0; i < size; ++i) {
            //array of numbers apart by shift
            numbers[i] = i * shift;
        }
        
        shuffleArray(numbers);
        
        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Build Map
        for (int i = 0; i < size; ++i) {
            int key = numbers[i];
            map.put(key, Math.random());
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        // Get values
        for (int i = 0; i < size; ++i) {
           int key = numbers[i];
           suma+= map.get(key);
            
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds

        
        for (int i = 0; i < size; ++i) {
                int key = numbers[i];
                map.remove(key);            
        }

        ellapsed.add(System.nanoTime() / 1e6); // Take time in milliSeconds
        
        ellapsed.add(suma);
        return ellapsed;
    }
	
	static void shuffleArray(int[] ar)
	  {
	
	    Random rnd = new Random();
	    for (int i = ar.length - 1; i > 0; i--)
	    {
	      Integer index = rnd.nextInt(i + 1);
	      // Simple swap
	      int a = ar[index];
	      ar[index] = ar[i];
	      ar[i] = a;
	    }
	  }
	
    public static void main(String[] args) {
        int logSize = Integer.parseInt(args[0]);
        int shift = Integer.parseInt(args[1]);
        ArrayList<Double> ellapsed = null;
        

         Map<Integer, Double> map;           
        map = new HashMap<>(1048576);
        ellapsed = evaluateInt(map, logSize, shift);
                
           
        

        System.out.print(logSize+"\t"+shift+"\t");
        for ( int i = 1; i < ellapsed.size(); ++i ) {
        	double res = (ellapsed.get(i) - ellapsed.get(i-1));
            System.out.print(res + "\t");
        }
        System.out.println();
    }

}
