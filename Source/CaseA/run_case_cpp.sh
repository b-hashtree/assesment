#!/bin/bash

printf "" > datCppA.tsv
printf "" > datCppA2.tsv

CPP_EXE=CppA

if [ ! -f "$CPP_EXE" ] ; then
    echo "[run_case_cpp.sh] Compiling"
    g++ -Wall -O3 CppA.cpp -o "$CPP_EXE"
fi

if [ CppA.cpp -nt "$CPP_EXE" ] ; then
    echo "[run_case_cpp.sh] Compiling"
    g++ -Wall -O3 CppA.cpp -o "$CPP_EXE"
fi

x=1
while [ $x -le 33 ]
do
echo "[run_case_cpp.sh] Processing: $CPP_EXE 20 $x >> datCppA.tsv 2>> logCppA.log"
printf "20\t$x\t" >> datCppA.tsv
./"$CPP_EXE" 16 $x >> datCppA.tsv 2>> logCppA.log
x=$(( $x + 1 ))
done 
 
for i in 2 4 8 16 32 64 128 256 512 1024
do
echo "[run_case_cpp.sh] Processing: $CPP_EXE 20 $i >> datCppA2.tsv 2>> logCppA2.log"
printf "20\t$i\t" >> datCppA2.tsv
./"$CPP_EXE" 16 $i >> datCppA2.tsv 2>> logCppA2.log
done  


