#!/usr/bin/env php

<?php

function evaluateInt($map, $logSize, $shift){
    $ellapsed = [];
    $suma=0;

    $ellapsed[]=microtime(true)*1000;
    $size = 1 << $logSize;
    $numbers=[];
    for($i=0;$i<$size;$i++){
        $numbers[]=$i*$shift;
    }
    shuffle($numbers);

    $ellapsed[]=microtime(true)*1000;

    for($i=0;$i<$size;$i++){
        $key=$numbers[$i];
        $map[$key]=1/rand(1,9);
    }

    $ellapsed[]=microtime(true)*1000;

    for($i=0;$i<$size;$i++){
        $key=$numbers[$i];
        $suma+=$map[$key];
    }

    $ellapsed[]=microtime(true)*1000;

    for($i=0;$i<$size;$i++){
        $key=$numbers[$i];
        unset($map[$key]);
    }

    $ellapsed[]=microtime(true)*1000;

    $ellapsed[]=$suma;



    return $ellapsed;
}

$map = [];
$logSize = $argv[1];
$shift = $argv[2];


$ellapsed = evaluateInt($map,$logSize,$shift);
print $logSize;
print "\t";
print $shift;
print "\t";
for($i=1;$i < count($ellapsed);$i++){
    $a= $ellapsed[$i];
    $b= $ellapsed[$i-1];
    print($a-$b);
    print "\t";
}
?>