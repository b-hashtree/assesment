﻿using System;
using System.Collections;
using System.Collections.Generic;

class CsA{
    public static ArrayList evaluateInt(Hashtable map, int logSize, int shift){
        ArrayList ellapsed = new ArrayList();
        float suma = 0;
        var rnd = new Random();
        var reloj = new System.Diagnostics.Stopwatch();
        //Build array
        reloj.Start();
        int size = 1 << logSize;
        int[] numbers = new int[size];
        for(int i = 0; i < size;i++){
            numbers[i]=i*shift;
        }
        reloj.Stop();
        //shuffleArray(numbers);
        ellapsed.Add(reloj.ElapsedMilliseconds);
        reloj.Reset();

        //Build Map
        reloj.Start();
        for (int i = 0; i < size; ++i) {
            int key = numbers[i];
            map.Add(key,rnd.NextSingle());
        }
        reloj.Stop();
        ellapsed.Add(reloj.ElapsedMilliseconds);
        reloj.Reset();

        //Get values
        reloj.Start();
        for (int i = 0; i < size; ++i) {
            int key = numbers[i];
            suma+= (float) map[key];

        }
        reloj.Stop();
        ellapsed.Add(reloj.ElapsedMilliseconds);
        reloj.Reset();

        //delete values
        reloj.Start();
        for (int i = 0; i < size; ++i) {
            int key = numbers[i];
            map.Remove(key);

        }
        reloj.Stop();
        ellapsed.Add(reloj.ElapsedMilliseconds);
        ellapsed.Add(suma);
        return ellapsed;
    }

    static void shuffleArray(int[] ar)
	  {
	
	    var rnd = new Random();
	    for (int i = ar.Length - 1; i > 0; i--)
	    {
	      long index = rnd.NextInt64(i + 1);
	      // Simple swap
	      int a = ar[index];
	      ar[index] = ar[i];
	      ar[i] = a;
	    }
	  }

    static void Main(string[] args){
        
        int logSize = Int32.Parse(args[0]);
        int shift = Int32.Parse(args[1]);
        Hashtable map = new Hashtable();
        ArrayList ellapsed = new ArrayList();
        ellapsed=evaluateInt(map,logSize,shift);

        Console.Write( logSize + "\t" + shift + "\t");
        foreach(var item in ellapsed )
        {
            Console.Write(item+"\t");
        }
        Console.WriteLine();
    }

}