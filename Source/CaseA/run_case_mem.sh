#!/bin/bash

printf "" > time.tsv

MEASURE="/bin/time -o time.tsv --append -f %M\t%e\t%S\t%U"

x=1
while [ $x -le 33 ]
do
printf "java\t$x\t" >> time.tsv
$MEASURE java JavaA 16 $x
x=$(( $x + 1 ))
done 

x=1
while [ $x -le 33 ]
do
printf "python\t$x\t" >> time.tsv
$MEASURE /bin/python3 PythonA.py 16 $x
x=$(( $x + 1 ))
done 

x=1
while [ $x -le 33 ]
do
printf "Php\t$x\t" >> time.tsv
$MEASURE php PhpA.php 16 $x
x=$(( $x + 1 ))
done 

x=1
while [ $x -le 33 ]
do
printf "Cs\t$x\t" >> time.tsv
$MEASURE dotnet ./Cs/bin/Release/net6.0/Cs.dll 16 $x 
x=$(( $x + 1 ))
done 

x=1
while [ $x -le 33 ]
do
printf "Cpp\t$x\t" >> time.tsv
$MEASURE CppA 16 $x 
x=$(( $x + 1 ))
done 




