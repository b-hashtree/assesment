
public class CuckooTable<K extends DualHash, V> implements SimpleMap<K, V> {

    Object[] bucketOne = new Object[1024];
    Object[] bucketTwo = new Object[1024];

    @Override
    public V get(K key) {
        int h1 = key.hashCodeOne(bucketOne.length);
        // Student Code Starts Here
        Entry<K, V> entry = getFromOne(h1);
        if (entry != null && entry.key.equals(key)) {
            return entry.value;
        }
        int h2 = key.hashCodeTwo(bucketTwo.length);
        entry = getFromTwo(h2);
        if (entry != null && entry.key.equals(key)) {
            return entry.value;
        }
        return null;
        // Student Code Ends Here
    }

    @Override
    public V put(K key, V value) {
        Entry<K, V> entry = new Entry<>(key, value);
        return recursivePut(entry, 0);
    }

    @Override
    public V remove(K key) {
        // Student Code Starts Here
        int h1 = key.hashCodeOne(bucketOne.length);
        // Student Code Starts Here
        Entry<K, V> entry = getFromOne(h1);
        if (entry != null && entry.key.equals(key)) {
            bucketOne[h1] = null;
            return entry.value;
        }
        int h2 = key.hashCodeTwo(bucketTwo.length);
        entry = getFromTwo(h2);
        if (entry != null && entry.key.equals(key)) {
            bucketTwo[h2] = null;
            return entry.value;
        }
        return null;
        // Student Code Ends Here
    }

    private V recursivePut(Entry<K, V> entry, int count) {
        if (1 << count > bucketOne.length) {
            // System.out.println("REHASHING");
            rehashing(entry);
            return null;
        } else {
            // Student Code Starts Here
            int h1 = entry.key.hashCodeOne(bucketOne.length);
            Entry<K, V> entry1 = getFromOne(h1);
            if (entry1 != null && entry1.key.equals(entry.key)) {
                V result = entry1.value;
                entry1.value = entry.value;
                return result;
            }
            int h2 = entry.key.hashCodeTwo(bucketTwo.length);
            Entry<K, V> entry2 = getFromTwo(h2);
            if (entry2 != null && entry2.key.equals(entry.key)) {
                V result = entry2.value;
                entry2.value = entry.value;
                return result;
            }
            if (entry1 == null) {
                bucketOne[h1] = entry;
                return null;
            }
            if (entry2 == null) {
                bucketTwo[h2] = entry;
                return null;
            }
            bucketOne[h1] = entry;
            recursivePut(entry1, count + 1);
            return null;
            // Student Code Ends Here
        }
    }

    private void rehashing(Entry<K, V> lastPut) {
        // Resize buckets and put all entries again
        // including lastPut
        // Student Code Starts Here
        Object[] aux1 = bucketOne;
        Object[] aux2 = bucketTwo;

        bucketOne = new Object[2 * bucketOne.length];
        bucketTwo = new Object[2 * bucketTwo.length];

        for (Object obj: aux1) {
            if ( obj != null ) {
                @SuppressWarnings("unchecked")
                Entry<K, V> entry = (Entry<K, V>) obj;
                recursivePut(entry, 0);
            }
        }
        for (Object obj : aux2) {
            if (obj != null) {
                @SuppressWarnings("unchecked")
                Entry<K, V> entry = (Entry<K, V>) obj;
                recursivePut(entry, 0);
            }
        }
        // Insert argument from last call
        // System.err.println("lastPut " + lastPut.key);
        recursivePut(lastPut, 0);
        // Student Code Ends Here
    }

    private Entry<K, V> getFromOne(int index) {
        @SuppressWarnings("unchecked")
        Entry<K, V> entry = (Entry<K, V>) bucketOne[index];
        return entry;
    }

    private Entry<K, V> getFromTwo(int index) {
        @SuppressWarnings("unchecked")
        Entry<K, V> entry = (Entry<K, V>) bucketTwo[index];
        return entry;
    }
}

class Entry<K, V> {

    Entry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K key;
    public V value;
}