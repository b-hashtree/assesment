
public interface DualHash {
    // First hash function (could be hashCode)
    // Precondition: size is a power of two
    int hashCodeOne(int size);
    
    // Alternative hash function
    // Precondition: size is a power of two
    int hashCodeTwo(int size);
}
