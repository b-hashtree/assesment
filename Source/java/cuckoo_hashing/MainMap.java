import java.util.HashMap;
import java.util.Random;

public class MainMap {
    public static void main(String[] args) {
        HashMap<DummyKey, Integer> map = new HashMap<>();
        Random random = new Random();

        int shift = 12;
        int size = 1 << shift; // 2 ** shift
        int[] numbers = new int[size];

        // Build
        for (int i = 0; i < size; ++i) {
            numbers[i] = random.nextInt(4 * size);
            DummyKey key = new DummyKey(String.format("%d", numbers[i]));
            Integer previous = map.put(key, i);
            if (previous != null) {
                numbers[previous] = -1;
            }
        }

        // System.out.println(map);

        // Check put and get (when the key exists)
        for (int i = 0; i < size; ++i) {
            if (numbers[i] != -1) {
                DummyKey key = new DummyKey(String.format("%d", numbers[i]));
                Integer index = map.get(key);
                if (index != null) {
                    int value = numbers[index];
                    if (value != numbers[i]) {
                        System.out.println(String.format("Error: incorrect value", key));
                        System.exit(1);
                    }
                } else {
                    System.out.println(String.format("Error: key `%s` not found in index %d", key, i));
                    System.exit(1);
                }
            }
        }
        
        // Remove some () keys
        int numberOfRemovals = 1 << (shift-2);
        for (int i = 0; i < numberOfRemovals; ++i) {
            if (numbers[i] != -1) {
                DummyKey key = new DummyKey(String.format("%d", numbers[i]));
                Integer index = map.remove(key);
                if (index != null) {
                    int value = numbers[index];
                    if (value != numbers[i]) {
                        System.out.println(String.format("Error: incorrect value in removing", key));
                        System.exit(1);
                    }
                } else {
                    System.out.println(String.format("Error: key `%s` not found in index %d", key, i));
                    System.exit(1);
                }
            }
        }

        // Check element doesn't exist
        for (int i = 0; i < numberOfRemovals; ++i) {
            if (numbers[i] != -1) {
                DummyKey key = new DummyKey(String.format("%d", numbers[i]));
                Integer index = map.get(key);
                if (index != null) {
                    System.out.println(String.format("Error: key `%s` removed in index %d", key, i));
                    System.exit(1);
                }
                index = map.remove(key);
                if (index != null) {
                    System.out.println(String.format("Error: key `%s` already removed in index %d", key, i));
                    System.exit(1);
                }
            }
        }

    }
}
