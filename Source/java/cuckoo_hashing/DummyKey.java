
public class DummyKey implements DualHash {

    private String key;

    public DummyKey(String key) {
        this.key = key;
    }

    @Override
    public int hashCodeOne(int size) {
        return key.hashCode() & (size - 1);
    }

    @Override
    public int hashCodeTwo(int size) {
        if (key.length() > 1) {
            return key.substring(1).hashCode() & (size - 1);
        } else {
            return (key.hashCode() >> 10) & (size - 1);
        }
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof DummyKey) {
            DummyKey another = (DummyKey) object;
            return key.equals(another.key);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return key;
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }
}