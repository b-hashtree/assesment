
public interface SimpleMap<K, V> {

    // searches for an entry with the key, returns the value
    // or null if no entry with key is found
    V get(K key);

    // inserts an entry <key,value>; returns the
    // the previous value associated with key, or null
    // if none
    V put(K key, V value); 

    // removes an entry with the given key,
    // returning the associated value
    // or null if none
    V remove(K key);
}