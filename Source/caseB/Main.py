import time
import sys
import random

class PythonB:

    def evaluateInt(map, logSize, shift):
        ellapsed=[]
        ellapsed.append(time.time_ns()/1e6)

        size = 1 << logSize
        numbers=[]
        for i in range(0,size):
            numbers.append(random.randint(1,121)<<shift)
    
        

        ellapsed.append(time.time_ns()/1e6)

        for i in range(0,size):
            key = numbers[i]
            previous=map.get(key)
            map[key]=i
            if(previous!=None):
                numbers[previous]=-1
    
        ellapsed.append(time.time_ns()/1e6)

        for i in range(0,size):
            if(numbers[i]!=-1):
                key=numbers[i]
                index= map.get(key)
                if(index!=None):
                    if(index!=i):
                        print("Error incorrect value",file=sys.stderr)
                        SystemExit(1)
                else:
                    print("Key not found in index",file=sys.stderr)
                    SystemExit(1)

            

    
        ellapsed.append(time.time_ns()/1e6)
           
        numberOfRemovals = int(size / 4)
        for i in range(0,numberOfRemovals):
            if (numbers[i] != -1):
                key = numbers[i]
                index = map.pop(key)
                if(index!=None):
                    value=numbers[index]
                    if(value!=numbers[i]):
                        print("Error incorrect value in removing",file=sys.stderr)
                        SystemExit(1)
                else:
                    print("Key not found in index to remove",file=sys.stderr)
                    SystemExit(1)

        

        ellapsed.append(time.time_ns()/1e6)

        for i in range(0,numberOfRemovals):
            if(numbers[i]!=-1):
                key=numbers[i]
                index = map.get(key)
                if(index!=None):
                    print("Error key removed in index",file=sys.stderr)
                    SystemExit(1)
                index=map.pop(key)
                if(index!=None):
                    print("Error key already removed in index",file=sys.stderr)
                    SystemExit(1)


        ellapsed.append(time.time_ns()/1e6)

        return ellapsed
        

    
    


    map = {}
    logSize = int(sys.argv[1])
    shift = int(sys.argv[2])

   

    ellapsed = evaluateInt(map,logSize,shift)

    for x in range(1,len(ellapsed)):
            a = ellapsed[x]
            b = ellapsed[x-1]
            print(a-b)

