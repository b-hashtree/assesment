#!/bin/bash

echo "             MapType       Map Size     Duplicates     Shift        Build Vector           Build Map             Lookups          Remove(1/4) Lookups(for removed)"
echo "------------------------------------------------------------------------------------------------------------------------------------------------------------------"

LogSize=20

for i in 0 2 4 8 10 12
do
#               MapType   LogSize   Shift   KeyType
	java Main Hashtable  $LogSize      $i   Integer
done

echo ""

for i in 0 2 4 8 10 12
do
#               MapType   LogSize   Shift   KeyType
	java Main   HashMap  $LogSize      $i   Integer
done

echo ""

for i in 0 2 4 8 10 12
do
#               MapType   LogSize   Shift   KeyType
	java Main Hashtable  $LogSize      $i   String
done

echo ""

for i in 0 2 4 8 10 12
do
#               MapType   LogSize   Shift   KeyType
	java Main   HashMap  $LogSize      $i   String
done

