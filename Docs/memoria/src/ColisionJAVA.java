import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ColisionJAVA {

/* En este codigo he usado una lista para almacenar los datos, 
 * recurro a ella para meter los datos 
 * en el mapa y los datos se cogen en orden.
 * Las claves tienen un valor aleatorio entre 0-1 para 
 * que el compilador no obvio pasos ni elimine
 * ejecuciones
*/	
public static void main(String[] args) {

	double suma=0;
	int tam = (int) Math.pow(2, Integer.parseInt(args[0]));
	int desp = Integer.parseInt(args[1]);
		
	List<Integer> lista = new ArrayList<Integer>();
		
	for(int i =0; i<tam;i++) {
		lista.add(i*desp);
	}				
	HashMap<Integer, Double> mapa1 = new HashMap<Integer, Double>();
		
	for(int i =0; i<tam;i++) {
		mapa1.put(lista.get(i), Math.random());	
	}
	Collections.shuffle(lista);
	
	long start = System.nanoTime();
	for(int i =0; i<tam;i++) {		
		suma+= mapa1.get(lista.get(i));
	}
	long end = System.nanoTime();
		
	System.out.println((end-start) + "\t"+ ((end-start)/tam) +  "\t" + suma   );	
	mapa1.clear();
		
	}
		
	
	
}
